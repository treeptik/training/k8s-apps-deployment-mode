# Demonstration commands

## requirements

kubectl apply -f kubernetes/requirements/namespace.yml

## create_rolling_update

kubectl -n app apply -f kubernetes/rolling-update/deployment_blue.yml
kubectl -n app apply -f kubernetes/rolling-update/service.yml
kubectl -n app apply -f kubernetes/rolling-update/ingress.yml

## update_rolling_update_to_green

kubectl -n app apply -f kubernetes/rolling-update/deployment_green.yml

## update_rolling_update_to_blue

kubectl -n app apply -f kubernetes/rolling-update/deployment_blue.yml

## delete_rolling_update

kubectl -n app delete ingress grid
kubectl -n app delete service grid
kubectl -n app delete deployment grid

## create_blue_green

kubectl -n app apply -f kubernetes/blue-green/deployment_blue.yml
kubectl -n app apply -f kubernetes/blue-green/deployment_green.yml
kubectl -n app apply -f kubernetes/blue-green/service_blue.yml
kubectl -n app apply -f kubernetes/blue-green/service_green.yml
kubectl -n app apply -f kubernetes/blue-green/ingress_blue.yml

## update_blue_green_to_green

kubectl -n app apply -f kubernetes/blue-green/ingress_green.yml

## update_blue_green_to_blue

kubectl -n app apply -f kubernetes/blue-green/ingress_blue.yml

## delete_blue_green

kubectl -n app delete ingress grid
kubectl -n app delete service grid-green grid-blue
kubectl -n app delete deployment grid-green grid-blue

## create_canary_release

kubectl -n app apply -f kubernetes/canary-release/deployment_blue.yml
kubectl -n app apply -f kubernetes/canary-release/deployment_green.yml
kubectl -n app apply -f kubernetes/canary-release/service_blue.yml
kubectl -n app apply -f kubernetes/canary-release/service_green.yml
kubectl -n app apply -f kubernetes/canary-release/ingress.yml

## update_canary_release_configuration

blue=80
green=20
kubectl -n app patch ingress grid --patch '{ "metadata": { "annotations": { "traefik.ingress.kubernetes.io/service-weights": "grid-blue: $(blue)\ngrid-green: $(green)"} } }'

## delete_canary_release

kubectl -n app delete ingress grid
kubectl -n app delete service grid-green grid-blue
kubectl -n app delete deployment grid-green grid-blue
